import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Opt1Component } from './options/opt1/opt1.component';
import { Opt2Component } from './options/opt2/opt2.component';
import { Opt3Component } from './options/opt3/opt3.component';
import { Opt4Component } from './options/opt4/opt4.component';
import { Opt5Component } from './options/opt5/opt5.component';
import { Opt6Component } from './options/opt6/opt6.component';

const routes: Routes = [
  { path: '', redirectTo: '/opt1', pathMatch: 'full' },
  { path: 'opt1', component: Opt1Component },
  { path: 'opt2', component: Opt2Component },
  { path: 'opt3', component: Opt3Component },
  { path: 'opt4', component: Opt4Component },
  { path: 'opt5', component: Opt5Component },
  { path: 'opt6', component: Opt6Component },

];
@NgModule({
  imports: [
     RouterModule.forRoot(routes)    
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
