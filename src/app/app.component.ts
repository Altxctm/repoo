import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],  
})
export class AppComponent implements OnInit {
  title = 'app';
  public menuState: string;
 
 
  ngOnInit() {
    this.menuState = "open";
  }

  openCloseMenu(event) {
    if (event) {
      this.menuState = "open";
    } else {
      this.menuState = "close";
    }
  }
}
