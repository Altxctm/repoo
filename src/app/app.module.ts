import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClientXsrfModule  }    from '@angular/common/http';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { AppRoutingModule } from './/app-routing.module';
import { Opt1Component } from './options/opt1/opt1.component';
import { Opt2Component } from './options/opt2/opt2.component';
import { Opt3Component } from './options/opt3/opt3.component';
import { Opt4Component } from './options/opt4/opt4.component';
import { Opt5Component } from './options/opt5/opt5.component';
import { Opt6Component } from './options/opt6/opt6.component';
import { ProxyServiceService } from './proxy-service.service';
import {interceptor} from '../interceptor';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {HourService} from './services/hour.service';
import {MatExpansionModule} from '@angular/material/expansion';
import {customGroupBy} from './Pipes/groupByCustomPipe';
import {SumPipe} from './Pipes/sum';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    LeftMenuComponent,
    Opt1Component,
    Opt2Component,
    Opt3Component,
    Opt4Component,
    Opt5Component,
    Opt6Component,
    customGroupBy,
    SumPipe,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatTableModule,
    MatExpansionModule,  
    NgPipesModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'xsrf-token',
      headerName: 'XSRF-TOKEN',
    }),
  ],
  providers: [
    ProxyServiceService, 
    CookieService,
    HourService, 
   
    {
    provide: HTTP_INTERCEPTORS,
    useClass: interceptor,
    multi: true,  
    }
 ],
  bootstrap: [AppComponent]
})
export class AppModule { }
