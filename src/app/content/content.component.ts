import { Component, OnInit, Input } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
  animations: [
    trigger('moveWithMenu', [
      state('open', style({ left: '200px'})),
      state('close', style({ left: '60px' })),
      transition('open => close', animate("0.25s ease-in")),
      transition('close => open', animate("0.25s ease-in"))     
    ])
  ]
  
})
export class ContentComponent implements OnInit {
  private open:string="open";
  private close:string="close";
  @Input() menuState: string = this.open;

  constructor() { }

  ngOnInit() {
  }

}
