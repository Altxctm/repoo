import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ProxyServiceService } from '../proxy-service.service';
@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})



export class LeftMenuComponent implements OnInit {

  @Output() openOrClose: EventEmitter<boolean> = new EventEmitter<boolean>();
  public visible: boolean = true;
  public menuState: string;
  constructor(private proxy: ProxyServiceService) { }

  ngOnInit() {
	  if(1==1){
		  console.log('test')
	  }
  }

  closeOpenMenu() {
    this.proxy.getToken();
    this.visible = !this.visible;
      this.openOrClose.emit(this.visible);
      if(this.visible){
        this.menuState= "open";
      }else{
        this.menuState="close";
      }
  }
}
