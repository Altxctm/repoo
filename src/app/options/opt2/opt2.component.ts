import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import {Hour} from '../../models/hour-model'
import { HourService } from '../../services/hour.service';
import { Subscription ,  Observable } from 'rxjs';

@Component({
  selector: 'app-opt2',
  templateUrl: './opt2.component.html',
  styleUrls: ['./opt2.component.css'],

 
})

export class Opt2Component implements OnInit {

  private subscription: Subscription;
  public hours: Hour[]=[];
 
   
  constructor(private hourService: HourService) { }

  ngOnInit() {
    this.subscription = this.hourService.observableHoras
    .subscribe(item => {
    this.hours= item;
    });   

    //group Hours
 
  }

  gethours() {
    this.hourService.getAllhours();   
  
    
  }

  fillHoras() {
    this.hourService.fillHoras();
  }

  updateHour() {
    this.hourService.updateH();
  }


  

 

  
}
