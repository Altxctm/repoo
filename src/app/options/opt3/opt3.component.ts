import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
@Component({
  selector: 'app-opt3',
  templateUrl: './opt3.component.html',
  styleUrls: ['./opt3.component.css'],
  animations: [trigger('show', [
    transition('void => *', [
      style({
        opacity: 0
      }),
      animate('0.25s ease-in')
    ])
  ])
  ]
})
export class Opt3Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
