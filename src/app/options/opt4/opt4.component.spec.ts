import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Opt4Component } from './opt4.component';

describe('Opt4Component', () => {
  let component: Opt4Component;
  let fixture: ComponentFixture<Opt4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Opt4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Opt4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
