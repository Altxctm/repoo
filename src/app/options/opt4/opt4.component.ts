import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
@Component({
  selector: 'app-opt4',
  templateUrl: './opt4.component.html',
  styleUrls: ['./opt4.component.css'],
  animations: [trigger('show', [
    transition('void => *', [
      style({
        opacity: 0
      }),
      animate('0.25s ease-in')
    ])
  ])
  ]
})
export class Opt4Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
