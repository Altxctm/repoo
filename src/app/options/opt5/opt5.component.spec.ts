import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Opt5Component } from './opt5.component';

describe('Opt5Component', () => {
  let component: Opt5Component;
  let fixture: ComponentFixture<Opt5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Opt5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Opt5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
