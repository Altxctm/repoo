import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
@Component({
  selector: 'app-opt5',
  templateUrl: './opt5.component.html',
  styleUrls: ['./opt5.component.css'],
  animations: [trigger('show', [
    transition('void => *', [
      style({
        opacity: 0
      }),
      animate('0.25s ease-in')
    ])
	
	
	
	
  ])
  ]
})
export class Opt5Component implements OnInit {

  constructor() {
	  }

  ngOnInit() {
  }

}
