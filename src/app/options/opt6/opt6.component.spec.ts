import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Opt6Component } from './opt6.component';

describe('Opt6Component', () => {
  let component: Opt6Component;
  let fixture: ComponentFixture<Opt6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Opt6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Opt6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
