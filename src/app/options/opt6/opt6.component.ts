import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
@Component({
  selector: 'app-opt6',
  templateUrl: './opt6.component.html',
  styleUrls: ['./opt6.component.css'],
  animations: [trigger('show', [
    transition('void => *', [
      style({
        opacity: 0
      }),
      animate('0.25s ease-in')
    ])
  ])
  ]
})
export class Opt6Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
