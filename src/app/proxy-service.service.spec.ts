import { TestBed, inject } from '@angular/core/testing';

import { ProxyServiceService } from './proxy-service.service';

describe('ProxyServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProxyServiceService]
    });
  });

  it('should be created', inject([ProxyServiceService], (service: ProxyServiceService) => {
    expect(service).toBeTruthy();
  }));
});
