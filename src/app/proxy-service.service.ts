import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Hour } from './models/hour-model';

export let tok:string="";

@Injectable()
export class ProxyServiceService {


  constructor(private http: HttpClient) { }

  getToken(): number {

    this.http.get<any>("http://localhost:62011/api/antiforgerytoken").subscribe((res: Response) => {
      let response = res.valueOf();
      let token = response['antiForgeryToken'];    
      tok = token;      
    });

    let body = { id: "test" };
    this.http.post("http://localhost:62011/api/Test/?id=", body).subscribe();

    return 1;
  }

}
