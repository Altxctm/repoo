import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  BehaviorSubject } from 'rxjs';
import { Hour } from '../models/hour-model';

import 'rxjs/Rx';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class HourService {

  private horas: Hour[] = [];

  public observableHoras;

  constructor(private http: HttpClient) {

    this.observableHoras = new BehaviorSubject<Hour[]>(this.horas);
    this.getAllhours()
  }


  getAllhours() {
    this.http.get<Hour[]>("http://localhost:62011/api/GetHours").subscribe(
      data => {
      this.horas = data
      this.horas = data
      ,this.cambiaronLasHoras()
      }
    );
  }

  cambiaronLasHoras() {
    this.sortHoursByDate();
    this.observableHoras.next(this.horas);
  }

  public sortHoursByDate(): void {
    this.horas.sort((a: Hour, b: Hour) => {
        return new Date(b.Date).getDate() - new Date(a.Date).getTime() ;

    });
  }

  fillHoras() {
    this.http.get("http://localhost:62011/api/fillHours").subscribe();
  }

  updateH(){
    this.http.get("http://localhost:62011/api/UpdateHour").subscribe();
    this.horas.forEach(element => {
      if(element.Description == "hora 1"){
        element.Time = element.Time +1;
      }
    });
    this.cambiaronLasHoras()
  }

}
