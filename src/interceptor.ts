import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { tok} from './app/proxy-service.service';

@Injectable()
export class interceptor implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({           
            headers: req.headers.set('XSRF-TOKEN', tok),
            withCredentials: true
        });
        return next.handle(authReq);
    }

}